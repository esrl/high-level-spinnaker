import matplotlib.pyplot as plt
import numpy as np

from sys import maxint
from quantities import ms

def spike(pop, rows, cols, pan):
	ax = plt.subplot(rows, cols, pan)
	neo = pop.get_data(variables = ["spikes"])
	spiketrains = neo.segments[0].spiketrains
	ax.set_xlim(0, spiketrains[0].t_stop / ms)
	max_index = 0
	min_index = maxint
	for spiketrain in spiketrains:
		ax.plot(spiketrain, np.ones_like(spiketrain) * spiketrain.annotations['source_index'],'k.')
		max_index = max(max_index, spiketrain.annotations['source_index'])
		min_index = min(min_index, spiketrain.annotations['source_index'])

	ax.set_ylabel('Neuron ID')
	ax.set_xlabel("Time[ms]")
	ax.set_ylim(-0.5 + min_index, max_index + 0.5)
	if pop.label:
		plt.text(0.975, 0.95, pop.label + ': spikes', transform=ax.transAxes, ha='right', va='top', bbox=dict(facecolor='white', alpha=1.0))

def spike_array(array, rows, cols, pan, simtime):
	ax = plt.subplot(rows, cols, pan)
	ax.set_xlim(0, simtime)
	max_index = len(array[1]) - 1
	min_index = 0
	for i in range(0, len(array[1])):
		ax.plot(array[1][i], np.ones_like(array[1][i]) * i,'k.')

	ax.set_ylabel('Neuron ID')
	ax.set_xlabel("Time[ms]")
	ax.set_ylim(-0.5 + min_index, max_index + 0.5)
	print(min_index)
	print(max_index)
	plt.text(0.975, 0.95, array[0] + ': spikes', transform=ax.transAxes, ha='right', va='top', bbox=dict(facecolor='white', alpha=1.0))

def voltage(pop, rows, cols, pan):
	plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
	ax = plt.subplot(rows, cols, pan)
	neo = pop.get_data(variables = ["v"])
	v = neo.segments[0].filter(name = 'v')[0]
	ax.set_ylabel("Voltage")
	ax.set_xlabel("Time[ms]")
	ax.plot(v)
	ax.set_ylim(-70,-45)
	if pop.label:
		plt.text(0.975, 0.95, pop.label + ': voltage', transform=ax.transAxes, ha='right', va='top', bbox=dict(facecolor='white', alpha=1.0))

def pop(pop, rows, row):
	spike(pop, rows, 2, row * 2 + 1)
	voltage(pop, rows, 2, row * 2 + 2)

def array(array, rows, row, simtime):
	spike_array(array, rows, 2, row * 2 + 1, simtime)
#	voltage_array(None, rows, 2, row * 2 + 2)

def show():
	plt.show()

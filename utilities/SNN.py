import spynnaker8 as p
import plotter as plot
import matplotlib.pyplot as plt
import networkx as nx

class SpikingNeuralNetwork():

	def __init__(self, settings):
		self.simtime = settings["simtime"]
		self.timestep = settings["timestep"]
		self.simfreq = settings["simfreq"]
		self.g = nx.DiGraph()
		self.external_devices = []
		self.pops = {}
		self.pops_mon = {}
		self.sim_pops_mon = {}
		self.projs = {}
		p.setup(**settings)

	def pop(self, label, size, cellclass, *args, **kwargs):
		print("")
		print("Pop")
		print(label)
		print(size)
		print(cellclass)
		print(args)
		print(kwargs)
		args = (label,) + args
		kwargs['size'] = size
		kwargs['cellclass'] = cellclass
		self.g.add_node(*args, **kwargs)

	def proj(self, *args, **kwargs):
		print("proj")
		print(args)
		print(kwargs)
		if len(args) == 2 and kwargs == {}:
			kwargs['external'] = True
			self.g.add_edge(*args, **kwargs)
		else:
			kwargs['external'] = False
			self.g.add_edge(*args, **kwargs)

	def generate(self, *args, **kwargs):

		# Set PyNN labels on all pops
		for key, val in self.g.nodes().iteritems():
			self.g.nodes[key]['label'] = key

		# Populations
		for key, pop in self.g.nodes(data = True):
			# If monitor
			if 'mon' in pop and pop['mon']:
				del pop['mon']
				new_pop = p.Population(**pop)
				self.pops_mon[pop['label']] = new_pop
				self.pops[pop['label']] = new_pop
			# If input monitor
			elif 'sim_mon' in pop and pop['sim_mon']:
				del pop['sim_mon']
				new_pop = p.Population(**pop)
				self.sim_pops_mon[pop['label']] = new_pop
				self.pops[pop['label']] = new_pop
			else:
				if 'sim_mon' in pop:
					del pop['sim_mon']
				if 'mon' in pop:
					del pop['mon']
				new_pop = p.Population(**pop)
				self.pops[pop['label']] = new_pop

		# Projetions
		for source, dist, con in self.g.edges(data = True):
			if con['external']:
				p.external_devices.activate_live_output_to(self.pops[source], self.pops[dist])
			else:
				del con['external']
				self.projs[(source, dist)] = p.Projection(self.pops[source], self.pops[dist], **con)

		# Recording
		for key, val in self.pops_mon.iteritems():
			val.record(['spikes', 'v'])

	def run(self, *args, **kwargs):
		kwargs["simtime"] = self.simtime
		p.run(*args, **kwargs)

	def stop(self, *args, **kwargs):
		p.end()

	def stop_and_plot(self, *args, **kwargs):

		# Plot simulation inputs
		rows = len(self.pops_mon) + len(self.sim_pops_mon)
		keys = sorted(self.pops_mon.keys())
		sim_keys = sorted(self.sim_pops_mon.keys())
		
		for i, key in enumerate(sim_keys):
			plot.array((key, kwargs[key]), rows, i, self.simtime)

		# Collect and plot recordings
		for i, key in enumerate(keys):
			plot.pop(self.pops_mon[key], rows, i + len(self.sim_pops_mon))

		# Stop
		p.end()

		# Plot graph
		#pos = nx.circular_layout(self.g)

		#edges = self.g.edges()
		#edge_colors = ['g' if (not 'receptor_type' in self.g[u][v] or self.g[u][v]['receptor_type'] == 'excitatory') else 'r' for u, v in edges]
		#edge_labels = dict(zip(self.g.edges(), ['' if not 'synapse_type' in self.g[u][v] else ('w' + str(self.g[u][v]['synapse_type'].weight) + " : " + 'd' + str(self.g[u][v]['synapse_type'].delay)) for u, v in edges]))			
		
		#nodes = self.g.nodes()
		#node_colors = 'w'
		#node_labels = dict(self.g.nodes(data = 'label', default = '?'))

		#plt.subplot(rows,1,rows)
		#nx.draw(self.g, pos, edges = edges, edge_color = edge_colors, node_color = node_colors, labels = node_labels, node_size = 1000)
		#nx.draw_networkx_edge_labels(self.g, pos, edge_labels)
		
		plt.tight_layout()
		plt.show()
				 

import spynnaker8 as p
from spynnaker8.utilities import DataHolder as dh

from pacman.model.graphs.application import ApplicationSpiNNakerLinkVertex as sl_vertex
from pacman.model.constraints.key_allocator_constraints import FixedKeyAndMaskConstraint
from pacman.model.routing_info import BaseKeyAndMask

from spinn_front_end_common.abstract_models import AbstractProvidesOutgoingPartitionConstraints as opc
from spinn_front_end_common.abstract_models import AbstractSendMeMulticastCommandsVertex as mc_vertex
from spinn_front_end_common.utility_models.multi_cast_command import MultiCastCommand as mcc

from pacman.model.decorators import overrides

class MyDevice1(sl_vertex, mc_vertex, opc):
	def __init__(self, n_neurons = 16, spinnaker_link_id = 0, board_address = None, label = "spinlink1"):
		sl_vertex.__init__(self, n_atoms = n_neurons, spinnaker_link_id = spinnaker_link_id)
		opc.__init__(self)

	def get_outgoing_partition_constraints(self, partition):
		print("Partition")
		print (partition)
		return [FixedKeyAndMaskConstraint([BaseKeyAndMask(0xbeef0100, 0xFFFFFFF0)])]

	@property
	@overrides(mc_vertex.start_resume_commands)
	def start_resume_commands(self):
		return [mcc(key = 0xbeef0110, payload = 0, repeat = 5, delay_between_repeats = 100)]

	@property
	@overrides(mc_vertex.pause_stop_commands)
	def pause_stop_commands(self):
		return [mcc(key = 0xbeef0120, payload = 0, repeat = 5, delay_between_repeats = 100)]

	@property
	@overrides(mc_vertex.timed_commands)
	def timed_commands(self):
		return []
	
class MyDeviceDataHolder1(dh):

        def __init__(self, spinnaker_link_id = 1, keys = [], board_address = None, label = "MyDevice1"):
		dh.__init__(
			self, {
				"spinnaker_link_id": spinnaker_link_id,
				"board_address": board_address,
				"label": label
			})

	@staticmethod
	def build_model():
		return MyDevice1


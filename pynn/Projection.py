class Projection():
	
	def __init__(self, presynaptic_population, postsynaptic_population, connector, synapse_type = None, source = None, receptor_type = None, label = None):
		self.presynaptic_population = presynaptic_population
		self.postsynaptic_population = postsynaptic_population
		self.connector = connector
		self.synapse_type = synapse_type
		self.source = source
		self.receptor_type = receptor_type
		self.label = label

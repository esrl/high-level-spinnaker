class Population:

	def __init__(self, size, cellclass, cellparams = None, structure = None, initial_valus = {}, label = None, record = {}):
		self.size = size
		self.cellclass = cellclass
		self.cellparams = cellparams
		self.structure = structure
		self.initial_values = initial_values
		self.label = label
		self.record = record

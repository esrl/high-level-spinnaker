#include "../headers/UDPTransmitter.hpp"

#include <string.h>

UDPTransmitter::UDPTransmitter(std::string ip, uint16_t port){
  if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) throw 1;
  memset((char *) &other, 0, sizeof(other));
  other.sin_family = AF_INET;
  other.sin_port = htons(port);
  slen = sizeof(other);
  if (inet_aton(ip.c_str(), &other.sin_addr) == 0) throw 1;
}

void UDPTransmitter::transmit(char* data, uint16_t length){
  if (sendto(sock, data, length, 0, (struct sockaddr *) &other, slen) == -1) throw 1;
}

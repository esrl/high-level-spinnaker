#include "../headers/UDPTransmitter.hpp"
#include "../headers/SDPPacket.hpp"

#include <iomanip>

int main(int argc, char** argv){
  if (argc != 7){
    std::cout << "Usage: transmitter <ip> <udp port> <chipX> <chipY> <CPU> <port>" << std::endl;
    std::cout << "Try: transmitter 192.168.240.253 17893 1 1 7 1" << std::endl;
    exit(1);
  }
  UDPTransmitter transmitter(argv[1], atoi(argv[2]));

  char* data = "Syddansk universitet!!";
  SDPPacket packet;
  packet.setDest(atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]));
  packet.setCmd(0x0000, 0x0000, 0x00000000, 0x00000000, 0x00000000);
  packet.setData((void*)data, 36);

  std::cout << packet << std::endl;
  transmitter.transmit((char*)&packet, 46);
  transmitter.transmit((char*)&packet, 46);

  return 0;
}

#include "../headers/SDPPacket.hpp"

#include <iomanip>
#include <string.h>

#define PAD_0 0
#define PAD_1 1
#define FLAG 2
#define TAG 3
#define DEST_PORT_CPU 4
#define SOURCE_PORT_CPU 5
#define DEST_X 6
#define DEST_Y 7
#define SOURCE_X 8
#define SOURCE_Y 9

#define RETURN_CODE 10
#define SEQUENCE 12
#define ARG_1 14
#define ARG_2 18
#define ARG_3 22

#define DATA 26

SDPPacket::SDPPacket(){
  setFlagsTag(0x87, 0xff);
  setDest(0x00, 0x00, 0x01, 0x01);
  setSource(0x00, 0x00, 0xff, 0xff);
  setCmd(0x0000, 0x0000, 0x00000000, 0x00000000, 0x00000000);
}

void SDPPacket::setFlagsTag(uint8_t _flags, uint8_t _tag){
  data[FLAG] = _flags;
  data[TAG] = _tag;
}

void SDPPacket::setDest(uint8_t _destX, uint8_t _destY, uint8_t _destCPU, uint8_t _destPort){
  data[DEST_PORT_CPU] = (_destPort << 5) | (_destCPU & 0x1f);
  data[DEST_X] = _destX;
  data[DEST_Y] = _destY;
}

void SDPPacket::setSource(uint8_t _sourceX, uint8_t _sourceY, uint8_t _sourceCPU, uint8_t _sourcePort){
  data[SOURCE_PORT_CPU] = (_sourcePort << 5) | (_sourceCPU & 0x1f);
  data[SOURCE_X] = _sourceX;
  data[SOURCE_Y] = _sourceY;
}

void SDPPacket::setCmd(uint16_t _returnCode, uint16_t _sequence, uint32_t _arg1, uint32_t _arg2, uint32_t _arg3){
  uint16_t* ptr16;
  uint32_t* ptr32;
  ptr16 = (uint16_t*)&data[RETURN_CODE];
  *ptr16 = _returnCode;
  ptr16 = (uint16_t*)&data[SEQUENCE];
  *ptr16 = _sequence;
  ptr32 = (uint32_t*)&data[ARG_1];
  *ptr32 = _arg1;
  ptr32 = (uint32_t*)&data[ARG_2];
  *ptr32 = _arg2;
  ptr32 = (uint32_t*)&data[ARG_3];
  *ptr32 = _arg3;
}

void SDPPacket::setData(void* ptr, uint8_t size){
  memcpy(&data[DATA], ptr, size);
}

void SDPPacket::print(std::ostream& os){
  os << "=== SDP Header ===" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(1) << "Flags:      \t" << (int)data[PAD_0] << (int)data[PAD_1] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Tag:        \t" << (int)data[TAG] << std::endl;
  os << "------------\tX\tY\tCPU\tPort" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Destination:\t" << (int)data[DEST_X] << "\t" << (int)data[DEST_Y] << "\t" << (int)(data[DEST_PORT_CPU] & 0x1f) << "\t" << (int)(data[DEST_PORT_CPU] >> 5) << std::endl;
  os << std::hex << std::setfill('0') << std::setw(2) << "Source:     \t" << (int)data[SOURCE_X] << "\t" << (int)data[SOURCE_Y] << "\t" << (int)(data[SOURCE_PORT_CPU] & 0x1f) << "\t" << (int)(data[SOURCE_PORT_CPU] >> 5) << std::endl;
  os << "=== SCP Header ===" << std::endl;
  os << std::hex << std::setfill('0') << std::setw(4) << "Return code:\t" << (int)data[RETURN_CODE] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(4) << "Sequence:   \t" << (int)data[SEQUENCE] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 1:      \t" << (int)data[ARG_1] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 2:      \t" << (int)data[ARG_2] << std::endl;
  os << std::hex << std::setfill('0') << std::setw(8) << "Arg 3:      \t" << (int)data[ARG_3] << std::endl;
  os << "===    Data    ===" << std::endl;
  for (int i = 0; i < 16; i++){
    os << std::setfill('0') << std::setw(4) << i * 16 << "  ";
    for (int j = 0; j < 16; j++){
      os << std::hex << std::setfill('0') << std::setw(2) << (int)data[DATA + i * 16 + j] << " ";
    }
    os << std::endl;
  }
}

void SDPPacket::printRaw(){
  for (int i = 0; i < sizeof(SDPPacket); i++){
    if (i % 16 == 0) std::cout << std::setfill('0') << std::setw(4) << i * 16 << "  ";
      std::cout << std::hex << std::setfill('0') << std::setw(2) << (int)data[i] << " ";
    if (i % 16 == 15) std::cout << std::endl;
  }
  std::cout << std::endl;
}

std::ostream& operator<<(std::ostream& os, SDPPacket& packet){
  packet.print(os);
}

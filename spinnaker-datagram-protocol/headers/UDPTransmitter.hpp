#ifndef UDP_TRANSMITTER_HPP
#define UDP_TRANSMITTER_HPP

#include <stdint.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string>

#include <iostream>

class UDPTransmitter{

public:

  UDPTransmitter(std::string ip, uint16_t port);
  void transmit(char* data, uint16_t length);

private:

  struct sockaddr_in other;
  int sock;
  int slen;


};

#endif

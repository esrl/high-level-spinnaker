#ifndef SDP_TRANSMITTER_HPP
#define SDP_TRANSMITTER_HPP

#include "UDPTransmitter.hpp"

class SDPTransmitter{

public:

  SDPTransmitter();

private:

  UDPTransmitter* udp;

};

#endif

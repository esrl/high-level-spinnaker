from Neuron import Neuron
from Connection import Connection
from porter import load_json_spikes, save_json_spikes, load_json_network, save_json_network, load_json_simulation_settings

import os
import sys
sys.path.append(os.path.abspath('../utilities'))
from SNN import SpikingNeuralNetwork as SNN
from spinnaker_device import MyDeviceDataHolder as device
from SpinLink1 import MyDeviceDataHolder1 as device1
from SpinLink2 import MyDeviceDataHolder2 as device2
import spynnaker8 as p
import numpy as np

#######################
# Simulation settings #
#######################

network_file = "data/network.json"
settings_file = "data/settings.json"
input_file = "data/inputs.json"
output_file = "data/outputs.json"

# Setup network
neurons = {}
settings = {}
inputs = {}
outputs = {}

load_json_network(neurons, network_file)
load_json_simulation_settings(settings, settings_file)
load_json_spikes(inputs, input_file)

# Setup simulation
nn = SNN(settings)

# Simulation inputs
spike_trains = {}
for neuron_name in inputs:
	spike_trains[neuron_name] = []
	for i in range(0, len(inputs[neuron_name])):
		spike_trains[neuron_name].append(inputs[neuron_name][i])

# AER links
aer_dev = device()

# Populations
for neuron_name in neurons:
	neuron = neurons[neuron_name]

	if neuron.input:
		print("Creating input population: " + neuron_name)
		if neuron_name in spike_trains:
			nn.pop(neuron_name, neuron.size, cellclass = p.SpikeSourceArray(spike_times = spike_trains[neuron_name]), sim_mon = neuron.mon)
		else:
			print("Warning: Input population have no assigned simulation inputs")
			nn.pop(neuron_name, neuron.size, cellclass = p.SpikeSourceArray(spike_times = [[]]), sim_mon = neuron.mon)
	elif neuron.aer_input or neuron.aer_output:
		nn.pop(neuron_name, neuron.size, aer_dev, mon = False)

	else:
		print("Creating population: " + neuron_name)
		nn.pop(neuron.name, neuron.size, cellclass = getattr(p, neuron.model_name)(**neuron.model_parameters), mon = neuron.mon)

# Projections
for neuron_name in neurons:
	for con in neurons[neuron_name].tx_connections:
		print("Connecting \"" + con.tx.name + "\" to \"" + con.rx.name + "\"")
		if con.rx.aer_output:
			print("External")
			nn.proj(con.tx.name, con.rx.name)
		
		else:
			if con.weight < 0:
				nn.proj(con.tx.name, con.rx.name, connector = getattr(p, con.type)(**con.parameters), synapse_type = p.StaticSynapse(weight = -con.weight, delay = con.delay), receptor_type = 'inhibitory')
			else:
				print(con.type)
				nn.proj(con.tx.name, con.rx.name, connector = getattr(p, con.type)(**con.parameters), synapse_type = p.StaticSynapse(weight = con.weight, delay = con.delay), receptor_type = 'excitatory')
			
# Run simulation & plot / save outputs
nn.generate()
nn.run()
nn.stop_and_plot(**spike_trains)
save_json_spikes(outputs, output_file)

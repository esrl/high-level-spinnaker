from Visualizer import Visualizer
from Mouse import Mouse
from Keyboard import Keyboard
from Vector import Vector
import pygame as pg
from porter import load_json_network, save_json_network
from Neuron import Neuron
from Connection import Connection

neuron_type = [
	"IF_curr_exp",
	"IF_curr_alpha",
	"IF_cond_exp",
	"IF_cond_alpha",
	"IF_cond_exp_gsfa_grr",
	"EIF_cond_alpha_isfa_ista",
	"EIF_cond_exp_isfa_ista",
	"Izhikevich",
	"HH_cond_exp"
]
connection_type = [
	"AllToAllConnector",
	"OneToOneConnector",
	"FixedProbabilityConnector",
	None,
	None,
	None,
	None,
	None,
	None
]

def left_click(mouse, button, time):
	phys_pos = screen.map_screen_to_pos(mouse.pos)
	r = screen.neuron_radius

	con = Connection.collision_with_connection(neurons, phys_pos, r)

	if not Neuron.collision_with_neuron(neurons, phys_pos, r) == "":
		screen.selected = Neuron.collision_with_neuron(neurons, phys_pos, r)
		screen.selected_connection = None
	elif not con == None:
		screen.selected_connection = con
		screen.selected = ""
	elif Neuron.collision_with_neuron(neurons, phys_pos, r * 2) == "":
		Neuron.create_neuron(neurons, phys_pos)

def right_click(mouse, button, time):
	pos = screen.map_screen_to_pos(mouse.pos)
	r = screen.neuron_radius
	name = Neuron.collision_with_neuron(neurons, pos, r)
	if not name == "":
		if screen.selected == "":
			Neuron.delete_neuron(neurons, name)
		else:
			neurons[screen.selected].add_connection(neurons[name], 1, 1)
	else:
		screen.selected = ""

def move(mouse, pos, rel_pos, time):
	if mouse.button[MOUSE_MIDDLE]:
		screen.offset -= rel_pos
	elif mouse.button[MOUSE_LEFT]:
		r = screen.neuron_radius
		old_pos = screen.map_screen_to_pos(pos - rel_pos)
		name = Neuron.collision_with_neuron(neurons, old_pos, r)
		if (not name == "") and name == screen.selected:
			neurons[name].pos += screen.map_rel_screen_to_pos(rel_pos)	

def zoom_in(mouse, button, time):
	screen.scale *= 1.25

def zoom_out(mouse, button, time):
	screen.scale *= 0.8

def load_network(keyboard, button, time):
	print("Loading network from: network.json")
	load_json_network(neurons, "data/network.json")

def save_network(keyboard, button, time):
	print("Saving network to: network.json")
	save_json_network(neurons, "data/network.json")

def toggle_monitor(keyboard, button, time):
	if not screen.selected == "":
		if neurons[screen.selected].mon:
			print("Disabling monitoring on node: " + screen.selected)
		else:
			print("Enabling monitoring on node: " + screen.selected)
		neurons[screen.selected].mon = not neurons[screen.selected].mon

def toggle_input(keyboard, button, time):
	if not screen.selected == "":
		if not keyboard.button[pg.K_a]:
			if neurons[screen.selected].input:
				print("Disabling inputs on node: " + screen.selected)
			else:
				print("Enabling inputs on node: " + screen.selected)
			neurons[screen.selected].aer_input = False
			neurons[screen.selected].input = not neurons[screen.selected].input
		else:
			if neurons[screen.selected].aer_input:
				print("Disabling aer inputs on node: " + screen.selected)
			else:
				print("Enabling aer inputs on node: " + screen.selected)
			neurons[screen.selected].input = False
			neurons[screen.selected].aer_input = not neurons[screen.selected].aer_input
			

def toggle_output(keyboard, button, time):
	if not screen.selected == "":
		if not keyboard.button[pg.K_a]:
			if neurons[screen.selected].output:
				print("Disabling outputs on node: " + screen.selected)
			else:
				print("Enabling outputs on node: " + screen.selected)
			neurons[screen.selected].aer_output = False
			neurons[screen.selected].output = not neurons[screen.selected].output
		else:
			if neurons[screen.selected].aer_output:
				print("Disabling aer outputs on node: " + screen.selected)
			else:
				print("Enabling aer outputs on node: " + screen.selected)
			neurons[screen.selected].output = False
			neurons[screen.selected].aer_output = not neurons[screen.selected].aer_output

def up_key(keyboard, button, time):
	if not screen.selected == "":
		neurons[screen.selected].size += 1
		if neurons[screen.selected].size > 100:
			neurons[screen.selected].size = 100
		print("Population size of: " + screen.selected + " = " + str(neurons[screen.selected].size))
	elif not screen.selected_connection == None:
		con = screen.selected_connection
		con.weight += 1
		if con.weight > 100:
			con.weight = 100
		print("Connection weight for: " + con.tx.name + ", " + con.rx.name + " = " + str(con.weight))

def down_key(keyboard, button, time):
	if not screen.selected == "":
		neurons[screen.selected].size -= 1
		if neurons[screen.selected].size < 1:
			neurons[screen.selected].size = 1
		print("Population size of: " + screen.selected + " = " + str(neurons[screen.selected].size))
	elif not screen.selected_connection == None:
		con = screen.selected_connection
		con.weight -= 1
		if con.weight < -100:
			con.weight = -100
		print("Connection weight for: " + con.tx.name + ", " + con.rx.name + " = " + str(con.weight))

def left_key(keyboard, button, time):
	if not screen.selected_connection == None:
		con = screen.selected_connection
		con.delay -= 1
		if con.delay < 1:
			con.delay = 1
		print("Connection delay for: " + con.tx.name + ", " + con.rx.name + " = " + str(con.delay))
		
def right_key(keyboard, button, time):
	if not screen.selected_connection == None:
		con = screen.selected_connection
		con.delay += 1
		if con.delay > 100:
			con.delay = 100
		print("Connection delay for: " + con.tx.name + ", " + con.rx.name + " = " + str(con.delay))
		
def delete(keyboard, button, time):
	if not screen.selected_connection == None:
		pass

def exit(keyboard, button, time):
	running = False

def execute_simulation(keyboard, button, time):
	execfile("pynn.py")

def type_select(keyboard, button, time):
	if not screen.selected == "":
		if neuron_type[button - pg.K_1] is not None:
			neurons[screen.selected].type = neuron_type[button - pg.K_1]
			print("Neuron model for pop " + screen.selected + " = " + neuron_type[button - pg.K_1])
	elif not screen.selected_connection == None:
		con = screen.selected_connection
		if connection_type[button - pg.K_1] is not None:
			con.type = connection_type[button - pg.K_1]
			print("Connection: " + con.tx.name + " -> " + con.rx.name + ", type = " + con.type)

# graphics setup
pg.init()
size = Vector(1000, 1000)
screen = Visualizer(size, False)

# input interface setup
MOUSE_LEFT = int(1)
MOUSE_MIDDLE = int(2)
MOUSE_RIGHT = int(3)
MOUSE_WHEEL_UP = int(4)
MOUSE_WHEEL_DOWN = int(5)
MOUSE_WHEEL_LEFT = int(6)
MOUSE_WHEEL_RIGHT = int(7)

mouse = Mouse()
mouse.add_click_callback(MOUSE_LEFT, left_click) 
mouse.add_click_callback(MOUSE_RIGHT, right_click) 
mouse.add_click_callback(MOUSE_WHEEL_UP, zoom_in)
mouse.add_click_callback(MOUSE_WHEEL_DOWN, zoom_out)
mouse.add_move_callback(move)

keyboard = Keyboard()
keyboard.add_click_callback(pg.K_l, load_network)
keyboard.add_click_callback(pg.K_s, save_network)
keyboard.add_click_callback(pg.K_ESCAPE, exit)
keyboard.add_click_callback(pg.K_m, toggle_monitor)
keyboard.add_click_callback(pg.K_i, toggle_input)
keyboard.add_click_callback(pg.K_o, toggle_output)
keyboard.add_click_callback(pg.K_e, execute_simulation)

keyboard.add_click_callback(pg.K_1, type_select)
keyboard.add_click_callback(pg.K_2, type_select)
keyboard.add_click_callback(pg.K_3, type_select)
keyboard.add_click_callback(pg.K_4, type_select)
keyboard.add_click_callback(pg.K_5, type_select)
keyboard.add_click_callback(pg.K_6, type_select)
keyboard.add_click_callback(pg.K_7, type_select)
keyboard.add_click_callback(pg.K_8, type_select)
keyboard.add_click_callback(pg.K_9, type_select)

keyboard.add_click_callback(pg.K_DOWN, down_key)
keyboard.add_click_callback(pg.K_UP, up_key)
keyboard.add_click_callback(pg.K_LEFT, left_key)
keyboard.add_click_callback(pg.K_RIGHT, right_key)

#keyboard.add_click_callback(pg.K_DEL, delete)

# neural setup
neurons = {}

# main loop
running = True
while running:
	for event in pg.event.get():

		time = pg.time.get_ticks()

		if event.type == pg.QUIT:
			running = False

		elif event.type == pg.MOUSEBUTTONDOWN:
			mouse.down_event(event.button, time)

		elif event.type == pg.MOUSEBUTTONUP:
			mouse.up_event(event.button, time)

		elif event.type == pg.MOUSEMOTION:
			mouse.move_event(Vector(event.pos[0], event.pos[1]), time)

		elif event.type == pg.KEYDOWN:
			keyboard.down_event(event.key, time)

		elif event.type == pg.VIDEORESIZE:
			screen.resize(Vector(event.w, event.h))
	screen.draw_graph(neurons)

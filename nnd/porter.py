import json
from Vector import Vector
from Neuron import Neuron
from Connection import Connection

# imports the settings for the simulation
def load_json_simulation_settings(simulation_settings, filename):
	data = None

	with open(filename, 'r') as infile:
		data = json.load(infile)

	simulation_settings["timestep"] = data["timestep"]
	simulation_settings["simtime"] = data["simtime"]
	simulation_settings["simfreq"] = data["simfreq"]

# exports the settings of the simulation
def save_json_simulation():
	data = {}

	data["timestep"] = simulation_settings.timestep
	data["simtime"] = simulation_settings.simulation_time
	data["simfreq"] = simulation_settings.simulation_frequency

	with open(filename, 'w') as outfile:
		json_string = json.dumps(data, sort_keys = True, indent = 4)
		outfile.write(json_string)

# imports a json formated file including spike events
def load_json_spikes(spikes, filename):
	
	data = None
	with open(filename, 'r') as infile:
		data = json.load(infile)

	for pop_name in list(data["spike_times"]):
		spikes[pop_name] = data["spike_times"][pop_name]["spikes"]

# exports a json formated file including spike events
def save_json_spikes(spikes, filename):
	
	data = {}
	
	data["number_of_neurons"] = len(spikes)
	data["spike_times"] = {}
	
	for key in spikes:
		data["spike_times"][key] = {}
		data["spike_times"][key]["number_of_spikes"] = len(spikes[key])
		data["spike_times"][key]["spikes"] = spikes[key]

	with open(filename, 'w') as outfile:
		json_string = json.dumps(data, sort_keys = True, indent = 4)
		outfile.write(json_string)

# imports a json formatted file including a neural network
def load_json_network(pops, filename):

	data = None
	with open(filename, 'r') as infile:
		data = json.load(infile)

	for pop_name in list(data["population"]):
		json_pop = data["population"][pop_name]
		pops[pop_name] = Neuron(pop_name, Vector(json_pop["pos"][0], json_pop["pos"][1]))
		pops[pop_name].potential = json_pop["potential"]

		if json_pop["model_name"] == None:
			pops[pop_name].model_name = "IF_curr_exp"
		else:
			pops[pop_name].model_name = json_pop["model_name"]

		if json_pop["model_parameters"] == None:
			pops[pop_name].model_parameters = {}
		else:
			pops[pop_name].model_parameters = json_pop["model_parameters"]

		pops[pop_name].size = json_pop["size"]
		pops[pop_name].mon = json_pop["mon"]
		pops[pop_name].input = json_pop["input"]
		pops[pop_name].output = json_pop["output"]
		pops[pop_name].aer_input = json_pop["aer_input"]
		pops[pop_name].aer_output = json_pop["aer_output"]

	for pop_name in list(data["population"]):
		json_pop = data["population"][pop_name]
		for con in json_pop["tx_connection"]:
			connection = Connection(pops[con["tx"]], pops[con["rx"]], con["delay"], con["weight"])
			if con["type"] is None:
				connection.type = "AllToAllConnector"
			else:
				connection.type = con["type"]
			connection.parameters = con["parameters"]
			pops[pop_name].tx_connections.append(connection)
			print("Loaded connection")
			print(connection.type)
		for con in json_pop["rx_connection"]:
			connection = Connection(pops[con["tx"]], pops[con["rx"]], con["delay"], con["weight"])
			if con["type"] is None:
				connection.type = "AllToAllConnector"
			else:
				connection.type = con["type"]
			connection.parameters = con["parameters"]
			pops[pop_name].rx_connections.append(connection)
			print("Loaded connection")
			print(connection.type)

# exports a neural network as a json formatted file
def save_json_network(pops, filename):

	data = {}

	data["number_of_populations"] = len(pops)
	data["population"] = {}

	for pop_name in list(pops):

		data["population"][pop_name] = {}
		json_pop = data["population"][pop_name]
		pop = pops[pop_name]

		json_pop["pos"] = (pop.pos.x, pop.pos.y)
		json_pop["potential"] = pop.potential
		json_pop["model_name"] = pop.model_name
		json_pop["model_parameters"] = pop.model_parameters
		json_pop["mon"] = pop.mon
		json_pop["input"] = pop.input
		json_pop["output"] = pop.output
		json_pop["aer_input"] = pop.aer_input
		json_pop["aer_output"] = pop.aer_output
		json_pop["size"] = pop.size
		json_pop["tx_connection"] = []
		json_pop["rx_connection"] = []

		for connection in pop.tx_connections:

			con = {}
			con["tx"] = connection.tx.name
			con["rx"] = connection.rx.name
			con["weight"] = connection.weight
			con["delay"] = connection.delay
			con["type"] = connection.type
			con["parameters"] = connection.parameters

			json_pop["tx_connection"].append(con)

		for connection in pop.rx_connections:

			con = {}
			con["tx"] = connection.tx.name
			con["rx"] = connection.rx.name
			con["weight"] = connection.weight
			con["delay"] = connection.delay
			con["type"] = connection.type
			con["parameters"] = connection.parameters

			json_pop["rx_connection"].append(con)

	with open(filename, 'w') as outfile:
		json_string = json.dumps(data, sort_keys = True, indent = 4)
		outfile.write(json_string)

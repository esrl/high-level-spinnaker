from Vector import Vector

class Connection:
        
	def __init__(self, tx, rx, delay, weight):
		self.tx = tx
		self.rx = rx;
		self.delay = delay
		self.weight = weight
		self.type = None
		self.parameters = {}
	
	@staticmethod
	def collision_with_connection(neurons, pos, r):
		cons = []
		for key in neurons:
			n = neurons[key]
			for con in n.tx_connections:
				dist =  Vector.dist_to_line(con.rx.pos, con.tx.pos, pos)
				if dist < r:
					cons.append((dist, con))
		cons.sort()
		if len(cons) == 0:
			return None
		else:
			return cons[0][1]

from Connection import Connection
import string

class Neuron:

	def __init__(self, name, pos, model_name = None, model_parameters = None, size = 1):
		self.name = name				# string	the name of the population
		self.pos = pos					# Vector	2d or 3d position of the neuron
		self.potential = 0				# [float]	current threshhold potentials of the population
		self.model_name = model_name	# string	the neuron model
		self.model_parameters = {}		# dict		the parameters of the neural model
		self.size = size				# int		size of population
		self.mon = False				# boolean 	stating whether the population should be monitored
		self.input = False				# boolean	is used for input
		self.output = False				# boolean	is used for output
		self.aer_input = False			# boolean	is used for input
		self.aer_output = False			# boolean	is used for output
		self.rx_connections = []		# [Connection]	list of ingoing connections
		self.tx_connections = []		# [Connection]	list of outgoing connections

	def add_connection(self, rx, delay, weight):
		if not self.connected(rx):
			connection = Connection(self, rx, delay, weight)
			self.tx_connections.append(connection)
			rx.rx_connections.append(connection)

	def remove_connections(self, neuron_name):
		i = 0
		while i < len(self.tx_connections):
			if self.tx_connections[i].rx.name == neuron_name:
				self.tx_connections.pop(i)
				i -+ 1
			i += 1
		i = 0
		while i < len(self.rx_connections):
			if self.rx_connections[i].tx.name == neuron_name:
				self.rx_connections.pop(i)
				i -= 1
			i += 1

	def connected(self, neuron_name):
		for con in self.tx_connections:
			if con.rx == neuron_name:
				return True
		return False
	
	@staticmethod
	def collision_with_neuron(neurons, pos, r):
		for neuron_name in neurons:
			if (pos - neurons[neuron_name].pos).get_length() < r:
				return neuron_name
		return ""

	@staticmethod
	def create_neuron(neurons, pos):
		alphabet = list(string.ascii_lowercase)
		for letter in alphabet:
			if not letter in neurons:
				neurons[letter] = Neuron(letter, pos)
				return

	@staticmethod
	def delete_neuron(neurons, neuron_name):
		for name in neurons:
			neurons[name].remove_connections(neuron_name)
		del neurons[neuron_name]
	

import pygame as pg
from pygame import gfxdraw
from Neuron import Neuron
from Connection import Connection
from math import cos, sin, atan, pi, sqrt
from Vector import Vector

class Visualizer:
	
	def __init__(self, size, floating = True):
		self.selected = ""
		self.selected_connection = None
		self.scale = 1.0
		self.size = size
		self.offset = Vector(0, 0)
		self.neuron_radius = 10
		self.arrow_height = 10
		self.neuron_color = pg.Color(255, 0, 0)
		if floating:
			self.surface = pg.display.set_mode((self.size.x, self.size.y))
		else:
			self.surface = pg.display.set_mode((self.size.x, self.size.y), pg.RESIZABLE)
		pg.display.set_caption("Neural Network Designer")	

	def resize(self, size):
		self.size = size
		temp = self.surface
		self.surface = pg.display.set_mode((self.size.x, self.size.y), pg.RESIZABLE)
		self.surface.blit(temp, (0, 0))
		del temp	

	def draw_arrow(self, pos, direction, color):

		direction -= pi / 4		
		init_pol = ((0, 0), (cos(direction) * self.arrow_height * self.scale, sin(direction) * self.arrow_height * self.scale), (-sin(direction) * self.arrow_height * self.scale, cos(direction) * self.arrow_height * self.scale))
		pol = ((init_pol[0][0] + pos.x, init_pol[0][1] + pos.y), (init_pol[1][0] + pos.x, init_pol[1][1] + pos.y), (init_pol[2][0] + pos.x, init_pol[2][1] + pos.y))	
		pg.draw.polygon(self.surface, color, pol)

	def draw_neuron(self, neurons, neuron_name):
		pos = self.map_pos_to_screen(neurons[neuron_name].pos)
		radius = self.neuron_radius * self.scale
		color = self.neuron_color
		font = pg.font.SysFont("Comic sans MS", int(self.scale * 20))
		textsurface = font.render(neuron_name, False, (0, 0, 0))
		temp_size = textsurface.get_size()
		textsize = Vector(temp_size[0], temp_size[1])
		pos_text = pos - textsize / 2.
		self.surface.blit(textsurface, (pos_text.x, pos_text.y))

		if neuron_name == self.selected:	
			pg.gfxdraw.aacircle(self.surface, int(pos.x), int(pos.y), int(radius), pg.Color(0, 0, 0))	

	def draw_arrows(self, neurons, neuron_name):
		tx_neuron = neurons[neuron_name]
		for i in range(0, len(neurons[neuron_name].tx_connections)):
			tx_con = neurons[neuron_name].tx_connections[i]
			
			color = pg.Color(0, 255, 0)
			if tx_con.weight < 0:
				color = pg.Color(255, 0, 0)

			if tx_con.rx == tx_con.tx:
				offset = Vector(-1, -1)
				offset.scale_to(self.neuron_radius)
				pos = self.map_pos_to_screen(tx_con.tx.pos + offset)
				self.draw_arrow(pos, pi * 1.25, color)
			else:
				offset = tx_con.tx.pos - tx_con.rx.pos
				offset.scale_to(self.neuron_radius)

				tx_pos_arrow = self.map_pos_to_screen(tx_con.tx.pos - offset)
				rx_pos_arrow = self.map_pos_to_screen(tx_con.rx.pos + offset)

				direction = offset.get_angle()

				self.draw_arrow(rx_pos_arrow, direction, color)

	def draw_connections(self, neurons, neuron_name):
		tx_neuron = neurons[neuron_name]
		for i in range(0, len(neurons[neuron_name].tx_connections)):
			tx_con = neurons[neuron_name].tx_connections[i]
			
			selected_color = pg.Color(128, 128, 128)
			color = pg.Color(0, 0, 0)

			if tx_con.rx == tx_con.tx:
				r = (self.neuron_radius + self.arrow_height) * self.scale
				pos = (tx_con.tx.pos + self.offset) * self.scale - Vector(0, sqrt(pow(self.neuron_radius + self.arrow_height, 2) + pow(self.neuron_radius + self.arrow_height, 2))) * self.scale + self.size / 2.
				pos = self.map_pos_to_screen(tx_con.tx.pos) - Vector(0, sqrt(pow(self.neuron_radius + self.arrow_height, 2) + pow(self.neuron_radius + self.arrow_height, 2))) * self.scale
				pg.draw.arc(self.surface, color, (pos.x - r, pos.y - r, r * 2, r * 2), 1.75 * pi, 1.25 * pi, int(3 * self.scale))
			else:
				diff = tx_con.tx.pos - tx_con.rx.pos
				diff.scale_to(diff.get_length() - self.neuron_radius * 2)
				offset = diff
				offset.scale_to(self.neuron_radius)
			
				tx_pos = self.map_pos_to_screen(tx_con.tx.pos - offset * 1.5)
				rx_pos = self.map_pos_to_screen(tx_con.rx.pos + offset * 1.5)

				if self.selected_connection == tx_con:
					pg.draw.line(self.surface, selected_color, (tx_pos.x, tx_pos.y), (rx_pos.x, rx_pos.y), int(3 * self.scale))
				else:
					pg.draw.line(self.surface, color, (tx_pos.x, tx_pos.y), (rx_pos.x, rx_pos.y), int(3 * self.scale))

	def draw_graph(self, neurons):
		
		# Apply background
		bg_rect = pg.Rect((0, 0), (self.size.x, self.size.y))
		pg.draw.rect(self.surface, pg.Color(255, 255, 255), bg_rect, 0)
		
		# draw all neurons
		for neuron_name in neurons:
			self.draw_neuron(neurons, neuron_name)

		# draw all connections
		for neuron_name in neurons:
			self.draw_connections(neurons, neuron_name)

		# draw all arrows
		for neuron_name in neurons:
			self.draw_arrows(neurons, neuron_name)

		# flip screen buffers
		pg.display.flip()

	def map_pos_to_screen(self, pos):
		return (pos ) * self.scale + self.size / 2 - self.offset

	def map_screen_to_pos(self, pos):
		return (pos - self.size / 2 + self.offset) / self.scale
	
	def map_rel_pos_to_screen(self, pos):
		return pos * self.scale

	def map_rel_screen_to_pos(self, pos):
		return pos / self.scale

class Keyboard:

	def __init__(self):
		self.click_callback = [None] * 512
		self.release_callback = [None] * 512
		self.button = [False] * 512

	def add_click_callback(self, button, callback):
		self.click_callback[button] = callback

	def add_release_callback(self, button, callback):
		self.release_callback[button] = callback

	def down_event(self, button, time):
		self.button[button] = True
		if not self.click_callback[button] is None:
			self.click_callback[button](self, button, time)

	def up_event(self, button, time):
		self.button[button] = False
		if not self.release_callback[button] is None:
			self.release_callback[button](self, button, time)

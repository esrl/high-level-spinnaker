from math import atan, pi, cos, sin, sqrt, pow, fabs

class Vector:

	def __init__(self, x = 0, y = 0, z = 0):
		self.x = x
		self.y = y
		self.z = z

	def set_values(self, x = 0, y = 0, z = 0):
		self.x = x
		self.y = y
		self.z = z

	def constrain(self, x1 = 0, x2 = 0, y1 = 0, y2 = 0, z1 = 0, z2 = 0):
		if self.x < x1:
			self.x = x1
		elif self.x > x2:
			self.x = x2
		if self.y < y1:
			self.y = y1
		elif self.y > y2:
			self.y = y2
		if self.z < z1:
			self.z = z1
		elif self.z > z2:
			self.z = z2

	def get_length(self):
		return sqrt(pow(self.x, 2) + pow(self.y, 2) + pow(self.z, 2))
	def get_length_xy(self):
		return sqrt(pow(self.x, 2) + pow(self.y, 2))
	def get_length_xz(self):
		return sqrt(pow(self.x, 2) + pow(self.z, 2))
	def get_length_yz(self):
		return sqrt(pow(self.y, 2) + pow(self.z, 2))

	def scale_to(self, l = 0):
		if self.get_length() == 0:
			return
		scale = l / self.get_length()
		self.x *= scale
		self.y *= scale
		self.z *= scale
	def scale_to_x(self, x = 0):
		if self.x == 0:
			return
		scale = x / self.x
		self.x *= scale
		self.y *= scale
		self.z *= scale
	def scale_to_y(self, y = 0):
		if self.y == 0:
			return
		scale = y / self.y
		self.x *= scale
		self.y *= scale
		self.z *= scale
	def scale_to_z(self, z = 0):
		if self.z == 0:
			return
		scale = z / self.z
		self.x *= scale
		self.y *= scale
		self.z *= scale
		
	def rotate(self, rad):
		x = sin(-rad) * self.y + cos(-rad) * self.x
		y = cos(-rad) * self.y - sin(-rad) * self.x
		self.x = x
		self.y = y
	def rotate_x(self, rad):
		y = sin(-rad) * self.z + cos(-rad) * self.y
		z = cos(-rad) * self.z - sin(-rad) * self.y
		self.y = y
		self.z = z
	def rotate_y(self, rad):
		x = cos(-rad) * self.x - sin(-rad) * self.z
		z = sin(-rad) * self.x + cos(-rad) * self.z
		self.x = x
		self.z = z
	def rotate_z(self, rad):
		x = sin(-rad) * self.y + cos(-rad) * self.x
		y = cos(-rad) * self.y - sin(-rad) * self.x
		self.x = x
		self.y = y

	def get_angle(self):
		if self.y > 0:
			if self.x > 0:
				return atan(self.y / self.x)
			elif self.x < 0:
				return pi / 2 - atan(self.x / self.y)
			else:
				return pi / 2
		elif self.y < 0:
			if self.x > 0:
				return pi / 2 * 3 - atan(self.x / self.y)
			elif self.x < 0:
				return pi + atan(self.y / self.x)
			else:
				return pi / 2 * 3
		else:
			if self.x > 0:
				return 0
			elif self.x < 0:
				return pi
			else:
				return 0

	def get_scalar_product(self, vect):
		return self.x * vect.x + self.y * vect.y + self.z * vect.z

	def get_cross_product(self, vect):
		res = Vector()
		res.x = self.y * vect.z - self.z * vect.y
		res.y = self.z * vect.x - self.x * vect.z
		res.z = self.x * vect.y - self.y * vect.x
		return res

	@staticmethod	
	def dist_to_line(v, w, p):
		sl = pow(fabs(v.x - w.x), 2) + pow(fabs(v.y - w.y), 2)
		if sl == 0:
			return (p - v).get_length()
		t = max(0, min(1, (p - v).get_scalar_product(w - v) / sl))
		proj = v + (w - v) * t
		dist = (p - proj).get_length()
		return dist

	def __str__(self):
		return "{0}, {1}, {2}".format(self.x, self.y, self.z)

	def __add__(self, vect):
		new = Vector()
		new.x = self.x + vect.x
		new.y = self.y + vect.y
		new.z = self.z + vect.z
		return new

	def __sub__(self, vect):
		new = Vector()
		new.x = self.x - vect.x
		new.y = self.y - vect.y
		new.z = self.z - vect.z
		return new
		
	def __mul__(self, scale):
		new = Vector()
		new.x = self.x * scale
		new.y = self.y * scale
		new.z = self.z * scale
		return new

	def __div__(self, scale):
		new = Vector()
		new.x = self.x / scale
		new.y = self.y / scale
		new.z = self.z / scale
		return new

		

from Vector import Vector

class Mouse:

	def __init__(self):
		self.raw_callback = [None] * 20
		self.click_callback = [None] * 20
		self.double_callback = [None] * 20
		self.release_callback = [None] * 20

		self.button = [False] * 20
		
		self.move_callback = None

		self.pos = Vector(0, 0)
		self.last_pos = self.pos

		self.double_click_time = 50 # ms

		self.last_button = 0
		self.last_button_time = 0

	def add_raw_click_callback(self, button, callback):
		self.raw_callback[button] = callback

	def add_click_callback(self, button, callback):
		self.click_callback[button] = callback

	def add_double_click_callback(self, button, callback):
		self.double_callback[button] = callback

	def add_release_callback(self, button, callback):
		self.release_callback[button] = callback

	def down_event(self, button, time):
		self.button[button] = True

		if not self.raw_callback[button] is None:
			self.raw_callback[button](self, button, time)
		if button == self.last_button and time - self.double_click_time < self.double_click_time:
			self.last_button_time = 0
			if not self.double_callback[button] is None:
				self.double_callback[button](self, button, time)
		else:
			self.last_button = button
			self.last_button_time = time
			if not self.click_callback[button] is None:
				self.click_callback[button](self, button, time)

	def up_event(self, button, time):
		self.button[button] = False
		
		if not self.release_callback[button] is None:
			self.release_callback[button](self, button, time)

	def add_move_callback(self, callback):
		self.move_callback = callback

	def move_event(self, pos, time):
		self.last_pos = self.pos
		self.pos = pos
		if not self.move_callback is None:
			self.move_callback(self, pos, self.pos - self.last_pos, time)

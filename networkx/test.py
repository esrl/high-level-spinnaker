import os
import sys
sys.path.append(os.path.abspath('../utilities'))
from SNN import SpikingNeuralNetwork as SNN
import spynnaker8 as p
import numpy as np

simfreq = 150
timestep = 1.0
simtime = 100
n = 2

nn = SNN({"simtime": simtime, "timestep": timestep, "simfreq": simfreq})

# Simulation
spikes_a = np.random.randint(0, 1000, size = (n, 100))
spikes_b = np.random.randint(0, 1000, size = (n, 100))

# Populations
nn.pop('asim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_a), mon = False)
nn.pop('bsim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_b), mon = False)
nn.pop('a', size = n, cellclass = p.IF_curr_exp(tau_syn_E = 1.0), mon = True)
nn.pop('b', size = n, cellclass = p.IF_curr_exp(tau_syn_I = 1.0), mon = True)


# Projections
nn.proj('asim', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')
nn.proj('asim', 'b', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()

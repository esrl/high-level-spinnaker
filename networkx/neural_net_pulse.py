import os
import sys
sys.path.append(os.path.abspath('../utilities'))
from SNN import SpikingNeuralNetwork as SNN
import spynnaker8 as p
import numpy as np

########################
# Simulations settings #
########################

timestep = 1.0
simtime = 1000
simfreq = 150
n = 50

nn = SNN({"simtime": simtime, "timestep": timestep, "simfreq": simfreq})

# Simulation
spikes = np.concatenate((
	np.random.randint(simtime / 10 * 0, simtime / 10 * 1, size = (n, simtime * simfreq / 1000 / 10)),
	np.random.randint(simtime / 10 * 2, simtime / 10 * 3, size = (n, simtime * simfreq / 1000 / 10)),
	np.random.randint(simtime / 10 * 4, simtime / 10 * 5, size = (n, simtime * simfreq / 1000 / 10)),
	np.random.randint(simtime / 10 * 6, simtime / 10 * 7, size = (n, simtime * simfreq / 1000 / 10)),
	np.random.randint(simtime / 10 * 8, simtime / 10 * 9, size = (n, simtime * simfreq / 1000 / 10))
), axis = 1)

# Populations
nn.pop('sim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes), mon = False)
nn.pop('a', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('b', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('c', size = n, cellclass = p.IF_curr_exp(), mon = True)

# Projections
nn.proj('sim', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 5), receptor_type = 'excitatory')
nn.proj('a', 'b', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'excitatory')
nn.proj('a', 'c', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'excitatory')
nn.proj('b', 'c', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'inhibitory')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()

import os
import sys
sys.path.append(os.path.abspath('../utilities'))
from SNN import SpikingNeuralNetwork as SNN
import spynnaker8 as p
import numpy as np

simfreq = 150
timestep = 1.0
simtime = 1000
n = 50

nn = SNN({"simtime": simtime, "timestep": timestep, "simfreq": simfreq})

# Simulation
spikes_a =[] 
for i in range(0, n):
	spikes_a.append([])
	s = 0
	for j in range(0, 5):
		for k in range(0, j * 5):
			spikes_a[i].append(int(j * 250 + 250 / (j * 5) * k))
	s = s + j
print(spikes_a[0])
#spikes_b = [[]]
spikes_b = np.random.randint(300, 800, size = (n, 100))
#spikes_c = np.random.randint(0, 1000, size = (n, 50))
#spikes_d = np.random.randint(0, 1000, size = (n, 50))
#spikes_c = []
#spikes_d = []

print (spikes_b)
print (spikes_a)

# Populations
nn.pop('asim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_a), mon = False)
nn.pop('bsim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_b), mon = False)
#nn.pop('csim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_c), mon = False)
#nn.pop('dsim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes_d), mon = False)
nn.pop('a', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('b', size = n, cellclass = p.IF_curr_exp(), mon = True)
#nn.pop('c', size = n, cellclass = p.IF_curr_exp(), mon = True)
#nn.pop('d', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('e', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('f', size = n, cellclass = p.IF_curr_exp(), mon = True)

# Projections
nn.proj('asim', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')
nn.proj('bsim', 'b', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')
#nn.proj('csim', 'c', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')
#nn.proj('dsim', 'd', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10.0), receptor_type = 'excitatory')

nn.proj('a', 'e', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 50), receptor_type = 'excitatory')
#nn.proj('b', 'f', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 50), receptor_type = 'inhibitory')
#nn.proj('c', 'e', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 50), receptor_type = 'excitatory')
#nn.proj('b', 'f', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 50), receptor_type = 'excitatory')

nn.proj('e', 'f', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'excitatory')
nn.proj('f', 'e', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'excitatory')
nn.proj('e', 'e', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'inhibitory')
nn.proj('f', 'f', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 5), receptor_type = 'inhibitory')
# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()

import os
import sys
sys.path.append(os.path.abspath('../utilities'))
from SNN import SpikingNeuralNetwork as SNN
import spynnaker8 as p
import numpy as np

simfreq = 150
simtime = 1000
timestep = 1.0
n = 50

nn = SNN({"simtime": simtime, "timestep": timestep, "simfreq": simfreq})

# Simulation
spikes = np.random.randint(0, 1000, size = (n, 50))
for spike in spikes:
	sorted(spike
)
print spikes

# Populations
nn.pop('sim', size = n, cellclass = p.SpikeSourceArray(spike_times = spikes), mon = False)
nn.pop('a', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('b', size = n, cellclass = p.IF_curr_exp(), mon = True)
nn.pop('c', size = n, cellclass = p.IF_curr_exp(), mon = True)

# Projections
nn.proj('sim', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10), receptor_type = 'excitatory')
nn.proj('a', 'b', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 10), receptor_type = 'excitatory')
nn.proj('b', 'c', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 10), receptor_type = 'excitatory')
nn.proj('c', 'b', connector = p.FixedProbabilityConnector(p_connect = 0.5), synapse_type = p.StaticSynapse(weight = 10.0 / n, delay = 10), receptor_type = 'inhibitory')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()

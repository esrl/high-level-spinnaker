# High-level Spinnaker

The repository includes examples and projects for PyNN and high-level spinnaker code.

In order to build and run these projects a repository for easy instalation of the tools is created. See [Spinnaker-setup](https://gitlab.esrl.dk/SDU-embedded/neuromorphic/Spinnaker-setup).

When the Spinnaker tools is set up, clone the repository into the shared folder.

    cd <spinnaker-setup>/shared/
    git clone https://gitlab.esrl.dk/SDU-embedded/neuromorphic/high-level-spinnaker.git
    
## High-level-tools usage (PyNN)

PyNN is a high level frame Python framework for neural simulation. SpyNNaker is a tool developped for the spinnaker board, using most of the functionality from PyNN. This means not all of the functionality of SpyNNaker comes from PyNN and not all the functionallity from PyNN is used in SpyNNaker.

For software related infomation, go for the PyNN documentation. http://neuralensemble.org/docs/PyNN/
For hardware related information, go for the spinnaker documentation. http://spinnakermanchester.github.io/

This reposetory consists of lots of examples using a small framework for plotting the results (networkx & matplotlib).
Furthermore there is a NeuralNetworkDesigner (./share/high-level/nnd/nnd.py) application where a network can be drawn from scratch and be simulated on the spinnaker system right away.

In the "external\_devices" folder (./share/high-level/external_devices/), three examples for AER io comunications is representet. One for only input, one for only output and one for both input and output.

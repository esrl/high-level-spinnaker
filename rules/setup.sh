#!/bin/sh

PYTHON_MODEL_TIMING_PATH=/usr/local/lib/python2.7/dist-packages/spynnaker8/models/synapse_dynamics/timing_dependence
PYTHON_MODEL_WEIGHT_PATH=/usr/local/lib/python2.7/dist-packages/spynnaker8/models/synapse_dynamics/weight_dependence
SPYNNAKER8_PATH=/usr/local/lib/python2.7/dist-packages/spynnaker8

C_MODEL_TIMING_PATH=/root/SupportScripts/sPyNNaker/neural_modelling/modified_src/neuron/plasticity/stdp/timing_dependence

MAKE_PATH=/root/SupportScripts/sPyNNaker/neural_modelling/makefiles/neuron/

cp ./timeing_dependence/__init__.py $PYTHON_MODEL_TIMING_PATH/__init__.py
cp ./weight_dependence/__init__.py $PYTHON_MODEL_WEIGHT_PATH/__init__.py
cp ./__init__.py $SPYNNAKER8_PATH/__init__.py
cp ./Makefile $MAKE_PATH/.

# model 1
MODEL=IF_curr_exp_stdp_mad_pair_custom_additive
cp ./models/$MODEL/timing_dependence_spike_pair_custom.py $PYTHON_MODEL_TIMING_PATH/.
cp ./models/$MODEL/timing_pair_custom_impl.c $C_MODEL_TIMING_PATH/.
cp ./models/$MODEL/timing_pair_custom_impl.h $C_MODEL_TIMING_PATH/.
mkdir $MAKE_PATH/$MODEL
cp ./models/$MODEL/Makefile $MAKE_PATH/$MODEL/.


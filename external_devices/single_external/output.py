import os
import sys
sys.path.append(os.path.abspath('../../utilities'))
from SNN import SpikingNeuralNetwork as SNN

import spynnaker8 as p
from spinnaker_device import MyDeviceDataHolder as device
import numpy as np

# Simulation settings
n = 4
settings = {
	"simtime": 10000,
	"timestep": 1.0,
	"simfreq": 1000
}

# Device
dev_out = device()

# Setup
nn = SNN(settings)

# Inputs
spikes = [
	np.arange(0, 10000, 1000) + 000,
	np.arange(0, 10000, 1000) + 250,
	np.arange(0, 10000, 1000) + 500,
	np.arange(0, 10000, 1000) + 750
]

# Populations
nn.pop('source', n, p.SpikeSourceArray(spikes), mon = False)
nn.pop('a', n, p.IF_curr_exp(), mon = True)
nn.pop('dev_out', n, dev_out, mon = False)

# Projections
nn.proj('source', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10), receptor_type = 'excitatory')
nn.proj('a', 'dev_out')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()

import os
import sys
sys.path.append(os.path.abspath('../../utilities'))
from SNN import SpikingNeuralNetwork as SNN

import spynnaker8 as p
from spinnaker_device import MyDeviceDataHolder as device
import numpy as np

# Simulation settings
n = 4
settings = {
	"simtime": 5000,
	"timestep": 1.0,
	"simfreq": 1000
}

# Device
dev_in = device()
dev_out = device()

# Setup
nn = SNN(settings)

# Populations
nn.pop('dev_in', n, dev_in, mon = False)
nn.pop('a', n, p.IF_curr_exp(), mon = True)
nn.pop('dev_out', n, dev_in, mon = False)

# Projections
nn.proj('dev_in', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10), receptor_type = 'excitatory')
nn.proj('a', 'dev_out')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()


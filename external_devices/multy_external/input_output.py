import os
import sys
sys.path.append(os.path.abspath('../../utilities'))
from SNN import SpikingNeuralNetwork as SNN

import spynnaker8 as p
#from spinnaker_device import MyDeviceDataHolder as device
from SpinLink0 import MyDeviceDataHolder0 as device0
from SpinLink1 import MyDeviceDataHolder1 as device1
import numpy as np

# Simulation settings
n = 4
settings = {
	"simtime": 10000,
	"timestep": 1.0,
	"simfreq": 1000
}

# Device
dev0_in = device0()
dev0_out = device0()

dev1_in = device1()
dev1_out = device1()

# Setup
nn = SNN(settings)

# Populations
nn.pop('dev0_in', n, dev0_in, mon = False)
nn.pop('a', n, p.IF_curr_exp(), mon = True)
nn.pop('dev0_out', n, dev0_in, mon = False)
nn.pop('dev1_in', n, dev1_in, mon = False)
nn.pop('b', n, p.IF_curr_exp(), mon = True)
nn.pop('dev1_out', n, dev1_in, mon = False)

# Projections
nn.proj('dev0_in', 'a', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10), receptor_type = 'excitatory')
nn.proj('a', 'dev0_out')
nn.proj('dev1_in', 'b', connector = p.OneToOneConnector(), synapse_type = p.StaticSynapse(weight = 5, delay = 10), receptor_type = 'excitatory')
nn.proj('b', 'dev1_out')

# Run simulation
nn.generate()
nn.run()
nn.stop_and_plot()


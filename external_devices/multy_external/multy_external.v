`timescale 1ns / 1ps

module
    top(
        input wire clk,
        input wire[3:0] btn,
        input wire[3:0] sw,
        output reg[3:0] led = 0,
        
        // RX
        input wire[6:0] rx0_tos,
        output wire rx0_tos_ack,
        input wire[6:0] rx1_tos,
        output wire rx1_tos_ack,
        
        // TX
        output wire[6:0] tx0_tos,
        input wire tx0_tos_ack,
        output wire[6:0] tx1_tos,
        input wire tx1_tos_ack
    );
    
    // input button debounce
    reg[3:0] dbtn = 0;
    reg[31:0] debounce1 = 0;
    reg[31:0] debounce2 = 0;
    reg[31:0] debounce3 = 0;
    reg[31:0] debounce4 = 0;
    
    always @(posedge clk) begin
        debounce1 = {debounce1[30:0], btn[0]};
        debounce2 = {debounce2[30:0], btn[1]};
        debounce3 = {debounce3[30:0], btn[2]};
        debounce4 = {debounce4[30:0], btn[3]};
    end
    
    always@(*) begin
        if (debounce1 == 32'h00000000) dbtn[0] = 0;
        else if (debounce1 == 32'hffffffff) dbtn[0] = 1;
        if (debounce2 == 32'h00000000) dbtn[1] = 0;
        else if (debounce2 == 32'hffffffff) dbtn[1] = 1;
        if (debounce3 == 32'h00000000) dbtn[2] = 0;
        else if (debounce3 == 32'hffffffff) dbtn[2] = 1;
        if (debounce4 == 32'h00000000) dbtn[3] = 0;
        else if (debounce4 == 32'hffffffff) dbtn[3] = 1;
    end
    
    wire clock;
    wire[15:0] scale;
    wire reset;
    wire state0;
    wire state1;
    
    assign clock = clk;
    assign scale = {sw, 4'h0};
    assign reset = sw[0];
    always @(*) led[3] = state0;
    always @(*) led[1] = state1;

    wire[31:0] rx0_key;
    wire[31:0] rx0_data;
    wire rx0_data_valid;
    wire rx0_cmd;
    wire rx0_act;

    wire[31:0] rx1_key;
    wire[31:0] rx1_data;
    wire rx1_data_valid;
    wire rx1_cmd;
    wire rx1_act;
    
    always @(*) led[2] = rx0_key[0];
    always @(*) led[0] = rx1_key[0];
    
    wire rx0_req;
    reg rx0_ack = 0;
    always @(clk) rx0_ack = rx0_req;
    
    wire rx1_req;
    reg rx1_ack = 0;
    always @(clk) rx1_ack = rx1_req;
    
    reg[31:0] tx0_key = 0;
    reg[31:0] tx0_data = 0;
    reg tx0_data_valid = 0;
    reg tx0_cmd = 0;
    wire tx0_act;

    always @(*) tx0_key = {31'h12340100, btn[1]};   
    
    reg tx0_req;
    wire tx0_ack;

    reg[31:0] tx1_key = 0;
    reg[31:0] tx1_data = 0;
    reg tx1_data_valid = 0;
    reg tx1_cmd = 0;
    wire tx1_act;
    
    always @(*) tx1_key = {31'h12340200, btn[3]};
    
    reg tx1_req;
    wire tx1_ack;
        
    always @(posedge clk) begin
        if (!tx0_ack && dbtn[2]) tx0_req = 1;
        else if (!dbtn[2]) tx0_req = 0;
        if (!tx1_ack && dbtn[0]) tx1_req = 1;
        else if (!dbtn[0]) tx1_req = 0;
    end
    
    spinlink #(
           .start_key(32'h12340001),
           .pause_key(32'h12340002)
       )
       sl0(
        .clock(clock),
        .clock_scale(scale),
        .reset(reset),
        .state(state0),
        
        .rx_key(rx0_key),
        .rx_data(rx0_data),
        .rx_data_valid(rx0_data_valid),
        .rx_cmd(rx0_cmd),
        .rx_act(rx0_act),
        
        .rx_req(rx0_req),
        .rx_ack(rx0_ack),
        
        .tx_key(tx0_key),
        .tx_data(tx0_data),
        .tx_data_valid(tx0_data_valid),
        .tx_cmd(tx0_cmd),
        .tx_act(tx0_act),
        
        .tx_req(tx0_req),
        .tx_ack(tx0_ack),
        
        .rx_tos(rx0_tos),
        .rx_tos_ack(rx0_tos_ack),
        
        .tx_tos(tx0_tos),
        .tx_tos_ack(tx0_tos_ack)
    );

    spinlink #(
        .start_key(32'h12340003),
        .pause_key(32'h12340004)
    )
    sl1(
        .clock(clock),
        .clock_scale(scale),
        .reset(reset),
        .state(state1),
        
        .rx_key(rx1_key),
        .rx_data(rx1_data),
        .rx_data_valid(rx1_data_valid),
        .rx_cmd(rx1_cmd),
        .rx_act(rx1_act),
        
        .rx_req(rx1_req),
        .rx_ack(rx1_ack),
        
        .tx_key(tx1_key),
        .tx_data(tx1_data),
        .tx_data_valid(tx1_data_valid),
        .tx_cmd(tx1_cmd),
        .tx_act(tx1_act),
        
        .tx_req(tx1_req),
        .tx_ack(tx1_ack),
        
        .rx_tos(rx1_tos),
        .rx_tos_ack(rx1_tos_ack),
        
        .tx_tos(tx1_tos),
        .tx_tos_ack(tx1_tos_ack)
    );
    
endmodule

module
    spinlink(
        
        // controll signals
        input wire clock,
        input wire[15:0] clock_scale, // internal_clock = clk / (2 * clock_scale) for clock_scale > 0
        input wire reset,
        output reg state = 0,
        
        ///////////////
        // FPGA side //
        ///////////////
        
        // RX
        output wire[31:0] rx_key,
        output wire[31:0] rx_data,
        output wire rx_data_valid,
        output wire rx_cmd,
        output wire rx_act,
        
        output wire rx_req,
        input wire rx_ack,
        
        // TX
        input wire[31:0] tx_key,
        input wire[31:0] tx_data,
        input wire tx_data_valid,
        input wire tx_cmd,
        input wire tx_act,

        input wire tx_req,
        output wire tx_ack,

        ///////////////////
        // SpinLink side //
        ///////////////////
        
        // RX
        input wire[6:0] rx_tos,
        output wire rx_tos_ack,
        
        // TX
        output wire[6:0] tx_tos,
        input wire tx_tos_ack
    );
    
    parameter start_key = 32'h12340001;
    parameter pause_key = 32'h12340002;
    
    reg[15:0] counter = 0;
    reg internal_clock = 0;
    
    always @(posedge clock) begin
        if (counter >= clock_scale) begin
            internal_clock = !internal_clock;
            counter = 0;
        end
        counter = counter + 1;
    end
    
    always @(posedge clock) begin
        if (rx_req && !rx_cmd) begin
            if (rx_key == start_key) state = 1;
            else if (rx_key == pause_key) state = 0;
        end
    end
    
    spinlink_tx tx(
        .clk(internal_clock),
        .rst(reset),
        
        .ack(tx_ack),
        .req(tx_req),
        .key(tx_key),
        .data(tx_data),
        .data_valid(tx_data_valid),
        .cmd(tx_cmd),
        .act(tx_act),
        
        .tos(tx_tos),
        .tos_ack(tx_tos_ack)
    );
    
    spinlink_rx rx(
        .clk(internal_clock),
        .rst(reset),
        
        .tos(rx_tos),
        .tos_ack(rx_tos_ack),
        
        .ack(rx_ack),
        .req(rx_req),
        .key(rx_key),
        .data(rx_data),
        .data_valid(rx_data_valid),
        .cmd(rx_cmd),
        .act(rx_act)
    );
    
endmodule

module
    spinlink_tx(
        // clock & reset
        input wire clk,
        input wire rst,
        
        // high-level interface
        output reg ack = 0,
        input wire req,
        input wire[31:0] key,
        input wire[31:0] data,
        input wire data_valid,
        input wire cmd,
        output reg act = 0,
        
        // low-level interface
        output reg[6:0] tos = 0,
        input wire tos_ack
    );
    
    // conversion table from 4 bit symbols to two-of-seven representation
    function[6:0] nibble2tos;
        input[3:0] nibble;
        begin
                 if (nibble ==  0) nibble2tos = 7'b0010001;
            else if (nibble ==  1) nibble2tos = 7'b0010010;
            else if (nibble ==  2) nibble2tos = 7'b0010100;
            else if (nibble ==  3) nibble2tos = 7'b0011000;
            else if (nibble ==  4) nibble2tos = 7'b0100001;
            else if (nibble ==  5) nibble2tos = 7'b0100010;
            else if (nibble ==  6) nibble2tos = 7'b0100100;
            else if (nibble ==  7) nibble2tos = 7'b0101000;
            else if (nibble ==  8) nibble2tos = 7'b1000001;
            else if (nibble ==  9) nibble2tos = 7'b1000010;
            else if (nibble == 10) nibble2tos = 7'b1000100;
            else if (nibble == 11) nibble2tos = 7'b1001000;
            else if (nibble == 12) nibble2tos = 7'b0010011;
            else if (nibble == 13) nibble2tos = 7'b0010110;
            else if (nibble == 14) nibble2tos = 7'b0011100;
            else if (nibble == 15) nibble2tos = 7'b0001001;
        end
    endfunction
    
    // 72-bit frame data holders
    reg[71:0] frame = 0;
    wire[71:0] temp_frame;
    // constuction the 72 bit package from data, key, cmd, data_valid, and the parity bit of the signals
    assign temp_frame = {data, key, cmd, 5'h00, data_valid, ~^temp_frame[71:1]};
    // state counter
    reg[4:0] nibble_counter = 0;
    // data holder for last tos to obtain the diference from prev time step
    reg last_tos_ack = 0;
        
    always @(posedge clk) begin
        
        // reset behavior
        if (rst) begin
            nibble_counter = 0;
            ack = 0;
            last_tos_ack = 0;
            frame = 0;
            act = 0;
            tos = 0;
        end
        else begin
            if (act) begin
                if (req == 0) ack = 0;
                
                // transmit end-of-packet symbol
                if ((nibble_counter == 10 && !frame[1]) || (nibble_counter == 18 && frame[1])) begin
                    if (last_tos_ack != tos_ack) begin
                        tos = tos ^ 7'b1100000;
                        last_tos_ack = tos_ack;
                        nibble_counter = 0;
                        act = 0;
                    end
                end
                
                // transmit next symbol
                else if (nibble_counter < 18) begin
                    if (last_tos_ack != tos_ack) begin
                        tos = tos ^ nibble2tos(frame[nibble_counter * 4 +: 4]);
                        last_tos_ack = tos_ack;
                        nibble_counter = nibble_counter + 1;
                    end
                end
                
                // end transmission
                else begin
                    nibble_counter = 0;
                    act = 0;
                end
            end
            
            // start transmission
            else begin
                if (req && !ack) begin
                    frame = temp_frame;
                    ack = 1;
                    act = 1;
                    nibble_counter = 0;
                end
                else if (req == 0) ack = 0;
            end
        end
    end
    
endmodule

module
    spinlink_rx(
        // clock & reset
        input wire clk,
        input wire rst,
        
        // low-level-interface
        input wire[6:0] tos,
        output reg tos_ack = 1,
        
        // high-level-interface
        input wire ack,
        output reg req,
        output reg[31:0] key,
        output reg[31:0] data,
        output reg data_valid,
        output reg cmd,
        output wire act
    );

    // conversion table from two-of-seven representation to 4 bit symbols
    function[4:0] tos2nibble;
        input[6:0] tos;
        begin
                 if (tos == 7'b0010001) tos2nibble = 5'b00000;
            else if (tos == 7'b0010010) tos2nibble = 5'b00001;
            else if (tos == 7'b0010100) tos2nibble = 5'b00010;
            else if (tos == 7'b0011000) tos2nibble = 5'b00011;
            else if (tos == 7'b0100001) tos2nibble = 5'b00100;
            else if (tos == 7'b0100010) tos2nibble = 5'b00101;
            else if (tos == 7'b0100100) tos2nibble = 5'b00110;
            else if (tos == 7'b0101000) tos2nibble = 5'b00111;
            else if (tos == 7'b1000001) tos2nibble = 5'b01000;
            else if (tos == 7'b1000010) tos2nibble = 5'b01001;
            else if (tos == 7'b1000100) tos2nibble = 5'b01010;
            else if (tos == 7'b1001000) tos2nibble = 5'b01011;
            else if (tos == 7'b0000011) tos2nibble = 5'b01100;
            else if (tos == 7'b0000110) tos2nibble = 5'b01101;
            else if (tos == 7'b0001100) tos2nibble = 5'b01110;
            else if (tos == 7'b0001001) tos2nibble = 5'b01111;
            else if (tos == 7'b1100000) tos2nibble = 5'b10000;
            else                        tos2nibble = 5'b11111;
        end
    endfunction
    
    reg[6:0] last_tos = 0;
    reg[6:0] buffer[1:0];
    initial buffer[0] = 0;
    initial buffer[1] = 0;
    wire[6:0] diff = buffer[1] ^ last_tos;
    
    reg[4:0] nibble_counter = 0;
    
    reg[71:0] frame = 0;
    
    assign act = (nibble_counter != 0) ? 1 : 0;
    
    wire[4:0] nibble = tos2nibble(diff);
    
    always @(posedge clk) begin
        if (rst) begin
            tos_ack = !tos_ack;
            req = 0;
            data_valid = 0;
            key = 0;
            data = 0;
            cmd = 0;
            nibble_counter = 0;
            frame = 0;
        end
        else if (req) begin
            if (ack) req = 0;
            frame = 0;
        end
        else begin
            if ((buffer[0] == buffer[1]) && (nibble != 5'b11111)) begin
                if (nibble != 5'b10000) begin
                    frame[nibble_counter * 4 +: 4] = nibble[3:0];
                    nibble_counter = nibble_counter + 1;
                end
                else begin
                    if (^frame) begin
                        if ((nibble_counter == 18) && frame[1]) begin;
                            data_valid = 1;
                            req = 1;
                        end
                        else if ((nibble_counter == 10) && !frame[1]) begin
                            data_valid = 0;
                            req = 1;
                        end
                        key = frame[39:8];
                        data = frame[71:40];
                        cmd = frame[7];
                    end
                    else frame = 0;
                    nibble_counter = 0;
                end
                tos_ack = !tos_ack;
                last_tos = tos;
            end
            buffer[1] = buffer[0];
            buffer[0] = tos;
        end
    end

endmodule
